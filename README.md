# Blender Batch Baker

## About

Blender Batch Baker is intended to automate the process of baking many objects with many materials in a collection.  It uses the cycles render engine to bake the objects in the selected collection creating new materials with one texture per object.  This can be useful for exporting path-traced lighting to web or game engine platforms where fast performance is required.

## Prerequisites

- You will need a copy of Blender which can be downloaded [here](https://www.blender.org/).  
- You will need a 3D scene with all the objects you would like to bake in a single collection.  
- You will also need a backup of your data, because this script will replace the existing materials with new ones.

## Usage 

1. Baking can take a long time, especially for multiple objects, so it is recommended to open [Blender's console window](https://i.stack.imgur.com/hK3bu.jpg) to monitor the baking process. To open Blender with console on MacOS or Linux, see [here](https://docs.blender.org/manual/en/dev/advanced/command_line/launch/index.html#command-line-launch-index).
2. To start, paste the contents of the file [batch_baker.py](batch_baker.py) into the [Blender text editor](https://docs.blender.org/manual/en/latest/editors/text_editor.html).
3. Next select the collection containing the objects you would like to bake in the outliner.
4. To start the baking process, run the script using the play button in the Blender text editor or by pressing Alt+P with your cursor inside the text editor.

## Notes and Limitations

- If an object does not have a material, a default material with a Principled BSDF will be created.
- This script will only bake the combined pass into a single emissive surface, it does not support separated normal, bump, or roughness maps.
- There is a [bug](https://projects.blender.org/blender/blender/issues/108171) in Blender 3.6.0 which causes this script to crash, it is advised to use a Blender version higher or lower than this.
