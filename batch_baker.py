# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# It is recommended that this script is used with Blender 3.6.1 or 
# later because of improvements to the UV unwrapping and packing 
# performance in these versions.

import bpy
import bmesh
import math
from datetime import datetime
from mathutils import Vector

# clear orphan data
bpy.ops.outliner.orphans_purge(do_local_ids=True, do_linked_ids=True, do_recursive=False)

scene = bpy.context.scene
act_coll = bpy.context.view_layer.active_layer_collection.collection
img_size = 4096
dynamic_textures = True
scene.cycles.device = 'GPU'
scene.cycles.samples = 64
scene.render.engine = 'CYCLES'
process_UV = True
scene.render.bake.margin = 3
pack_margin = 0.00025
length = len(act_coll.objects)
sizes = [1024,2048,4096,8192,16384]
i = 1

for obj in act_coll.objects:
    
    if obj.type == 'MESH':
    
        if dynamic_textures == True:
            mesh = obj.data
            total_area = 0
            for p in mesh.polygons:
                total_area += p.area
            img_size = min(sizes, key=lambda x:abs(x-total_area))
            square = img_size
        
        print('\n')
        print('Processing',i,'out of',length,'|', round(((i/length)*100),2),'%')       
        print('surface area:',total_area,'square metres.') 
        print('Using pixel dimension of',img_size)
        
        tex = bpy.data.images.new(name=obj.name,width=img_size,height=img_size,alpha=False,)
        
        if len(obj.data.materials) < 1:
            print('no materials found on', obj.name, 'adding a default material.')
            def_mat = bpy.data.materials.new(name=obj.name+'_temp')
            obj.data.materials.append(def_mat)            
        
        for mat in obj.data.materials:
            if mat is not None:
                if mat.use_nodes == False:
                    diff_col = mat.diffuse_color
                    mat.use_nodes = True
                    mat.node_tree.nodes["Principled BSDF"].inputs[0].default_value = diff_col
                node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
                node.image = tex
                node.location = Vector((-300, 200))
                node.select = True
                mat.node_tree.nodes.active = node
        
        for ob in bpy.context.selected_objects:
            ob.select_set(False)
        obj.select_set(True)
        bpy.context.view_layer.objects.active = obj
        
        if process_UV == True:        
            bpy.ops.object.editmode_toggle()
            bpy.ops.mesh.select_all(action='SELECT')
            bpy.ops.uv.smart_project(island_margin=0)
            bpy.ops.uv.select_all(action='SELECT')
            bpy.ops.uv.pack_islands(margin=pack_margin)
            bpy.ops.object.editmode_toggle()    
        
        now = datetime.now()
        current_time = now.strftime("%d/%m/%Y %H:%M:%S")
        print(current_time)
        print('Baking [',obj.name,'] this may take some time.')       
        
        try:
            bpy.ops.object.bake(type='COMBINED')
        except:
            print('There was a problem baking [',obj.name,']')
        
        obj.data.materials.clear()
        new_mat = bpy.data.materials.new(name=f'{obj.name}_material')
        obj.data.materials.append(new_mat)
        new_mat.use_nodes = True
        outputnode = new_mat.node_tree.nodes['Material Output']
        principlednode = new_mat.node_tree.nodes["Principled BSDF"]
        
        # add an emission shader
        emissionnode = new_mat.node_tree.nodes.new(type="ShaderNodeEmission")
        emissionnode.location = (-250, 0)
        
        # add image texture
        texturenode = new_mat.node_tree.nodes.new(type="ShaderNodeTexImage")
        texturenode.image = tex
        texturenode.location = (-600,300)
        
        # make links
        new_mat.node_tree.links.new(principlednode.outputs[0], outputnode.inputs[0])
        new_mat.node_tree.links.new(texturenode.outputs[0], principlednode.inputs[0])
        
        i+=1
    
print('\n')
    
print('Applying emissive materials.')

for obj in act_coll.objects:
    
    new_mat = obj.data.materials[0]
    
    emissionnode = new_mat.node_tree.nodes['Emission']
    texturenode = new_mat.node_tree.nodes['Image Texture']
    outputnode = new_mat.node_tree.nodes['Material Output']
    
    new_mat.node_tree.links.new(emissionnode.outputs[0], outputnode.inputs[0])
    new_mat.node_tree.links.new(texturenode.outputs[0], emissionnode.inputs[0])
    
now = datetime.now()
current_time = now.strftime("%d/%m/%Y %H:%M:%S")
print('Baking finished at', current_time)
